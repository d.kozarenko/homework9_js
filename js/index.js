"use strict";

const tab = document.querySelectorAll(".tabs-title");

if (!document.querySelector(`[data-tab-name ="Akali"`)) {
  document.querySelector('[data-name="Akali"]').classList.remove("active-text");
}

tab.forEach((element) => {
  if (
    document.querySelector('[data-name= "' + element.dataset.tabName + '"]')
  ) {
    element.addEventListener(`click`, buttonActivatingHandler);
  } else {
    element.classList.add("none");
  }
});

function loop() {
  tab.forEach((element) => {
    if (element.classList.contains("active")) {
      element.classList.remove("active");
      document
        .querySelector('[data-name= "' + element.dataset.tabName + '"]')
        .classList.remove("active-text");
    }
  });
}

function buttonActivatingHandler() {
  let currentText = document.querySelector(
    '[data-name= "' + this.dataset.tabName + '"]'
  );
  if (currentText.classList.contains("active-text")) {
    currentText.classList.remove("active-text");
    this.classList.remove("active");
  } else {
    loop();
    currentText.classList.add("active-text");
    this.classList.add("active");
  }
}
